# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-12-13 08:23-0500\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: search.js:656 searchGrid.js:694
msgid "Searching..."
msgstr ""

#: search.js:658 searchGrid.js:696
msgid "No results."
msgstr ""

#: search.js:733 searchGrid.js:788
#, javascript-format
msgid "%d more"
msgid_plural "%d more"
msgstr[0] ""
msgstr[1] ""

#: menulayouts/mint.js:244 menulayouts/arcmenu.js:264
#: menulayouts/redmond.js:440 menuWidgets.js:1118 prefs.js:2927
msgid "Terminal"
msgstr ""

#: menulayouts/mint.js:245 menulayouts/brisk.js:392 menulayouts/arcmenu.js:264
#: menulayouts/redmond.js:440 menulayouts/windows.js:264 menuWidgets.js:679
#: prefs.js:2927
msgid "Settings"
msgstr ""

#: menulayouts/mint.js:251 menulayouts/brisk.js:392 menulayouts/arcmenu.js:264
#: menulayouts/redmond.js:440 prefs.js:2927
msgid "Software"
msgstr ""

#: menulayouts/mint.js:252
msgid "Files"
msgstr ""

#: menulayouts/mint.js:253 menuWidgets.js:797 prefs.js:2998
msgid "Log Out"
msgstr ""

#: menulayouts/mint.js:254 menuWidgets.js:826 prefs.js:2981
msgid "Lock"
msgstr ""

#: menulayouts/mint.js:255 menuWidgets.js:785 prefs.js:3015
msgid "Power Off"
msgstr ""

#: menulayouts/tweaks/tweaks.js:84
msgid "Category Activation"
msgstr ""

#: menulayouts/tweaks/tweaks.js:90
msgid "Mouse Click"
msgstr ""

#: menulayouts/tweaks/tweaks.js:91
msgid "Mouse Hover"
msgstr ""

#: menulayouts/tweaks/tweaks.js:110
msgid "Avatar Icon Shape"
msgstr ""

#: menulayouts/tweaks/tweaks.js:115
msgid "Circular"
msgstr ""

#: menulayouts/tweaks/tweaks.js:116
msgid "Square"
msgstr ""

#: menulayouts/tweaks/tweaks.js:141
msgid "KRunner Position"
msgstr ""

#: menulayouts/tweaks/tweaks.js:146
msgid "Top"
msgstr ""

#: menulayouts/tweaks/tweaks.js:147
msgid "Centered"
msgstr ""

#: menulayouts/tweaks/tweaks.js:171 prefs.js:130 prefs.js:529
msgid "Save"
msgstr ""

#: menulayouts/tweaks/tweaks.js:197
msgid "Separator Position Index"
msgstr ""

#: menulayouts/tweaks/tweaks.js:385
msgid "Nothing Yet!"
msgstr ""

#: menulayouts/tweaks/tweaks.js:399 prefs.js:716
msgid "Arc Menu Default View"
msgstr ""

#: menulayouts/tweaks/tweaks.js:405 prefs.js:41 prefs.js:722
msgid "Pinned Apps"
msgstr ""

#: menulayouts/tweaks/tweaks.js:406 prefs.js:723
msgid "Categories List"
msgstr ""

#: menulayouts/arcmenu.js:209 menulayouts/redmond.js:385 prefs.js:2841
msgid "Network"
msgstr ""

#: menulayouts/arcmenu.js:264 menulayouts/redmond.js:440 prefs.js:1555
#: prefs.js:1581 prefs.js:1586 prefs.js:1618 prefs.js:1629 prefs.js:2927
msgid "Tweaks"
msgstr ""

#: menulayouts/arcmenu.js:512 menulayouts/redmond.js:309
#: menulayouts/ubuntudash.js:143 placeDisplay.js:454 prefs.js:2841
msgid "Home"
msgstr ""

#: menulayouts/arcmenu.js:519 menulayouts/redmond.js:316
#: menulayouts/windows.js:115 menulayouts/windows.js:259
#: menulayouts/ubuntudash.js:150 prefs.js:2841
msgid "Documents"
msgstr ""

#: menulayouts/arcmenu.js:519 menulayouts/redmond.js:316
#: menulayouts/ubuntudash.js:150 prefs.js:2841
msgid "Downloads"
msgstr ""

#: menulayouts/arcmenu.js:519 menulayouts/redmond.js:316
#: menulayouts/ubuntudash.js:150 prefs.js:2841
msgid "Music"
msgstr ""

#: menulayouts/arcmenu.js:519 menulayouts/redmond.js:316
#: menulayouts/ubuntudash.js:150 prefs.js:2841
msgid "Pictures"
msgstr ""

#: menulayouts/arcmenu.js:519 menulayouts/redmond.js:316
#: menulayouts/ubuntudash.js:150 prefs.js:2841
msgid "Videos"
msgstr ""

#: menuWidgets.js:124
msgid "Open Folder Location"
msgstr ""

#: menuWidgets.js:140
msgid "Current Windows:"
msgstr ""

#: menuWidgets.js:159
msgid "New Window"
msgstr ""

#: menuWidgets.js:169
msgid "Launch using Dedicated Graphics Card"
msgstr ""

#: menuWidgets.js:192
msgid "Remove from Favorites"
msgstr ""

#: menuWidgets.js:198
msgid "Add to Favorites"
msgstr ""

#: menuWidgets.js:219 menuWidgets.js:272
msgid "Unpin from Arc Menu"
msgstr ""

#: menuWidgets.js:234
msgid "Pin to Arc Menu"
msgstr ""

#: menuWidgets.js:247
msgid "Show Details"
msgstr ""

#: menuWidgets.js:426 prefs.js:2927
msgid "Activities Overview"
msgstr ""

#: menuWidgets.js:692 menuWidgets.js:1115 menu.js:559
msgid "Arc Menu Settings"
msgstr ""

#: menuWidgets.js:705 menuWidgets.js:1661 menuWidgets.js:1805
#: menuWidgets.js:1971
msgid "Favorites"
msgstr ""

#: menuWidgets.js:720
msgid "Users"
msgstr ""

#: menuWidgets.js:809 prefs.js:2964
msgid "Suspend"
msgstr ""

#: menuWidgets.js:849
msgid "Back"
msgstr ""

#: menuWidgets.js:909 menuWidgets.js:1661 menuWidgets.js:1805
#: menuWidgets.js:1971
msgid "All Programs"
msgstr ""

#: menuWidgets.js:1664 menuWidgets.js:1808 menuWidgets.js:1974
msgid "Frequent Apps"
msgstr ""

#: menuWidgets.js:2137
msgid "Type to search…"
msgstr ""

#: menuWidgets.js:2306
msgid "Applications"
msgstr ""

#: placeDisplay.js:140
#, javascript-format
msgid "Failed to launch “%s”"
msgstr ""

#: placeDisplay.js:155
#, javascript-format
msgid "Failed to mount volume for “%s”"
msgstr ""

#: placeDisplay.js:239 placeDisplay.js:263 prefs.js:2841
msgid "Computer"
msgstr ""

#: placeDisplay.js:328
#, javascript-format
msgid "Ejecting drive “%s” failed:"
msgstr ""

#: prefs.js:59
msgid "Add More Apps"
msgstr ""

#: prefs.js:99
msgid "Add Custom Shortcut"
msgstr ""

#: prefs.js:269
msgid "Change Selected Pinned App"
msgstr ""

#: prefs.js:271
msgid "Select Apps to add to Pinned Apps List"
msgstr ""

#: prefs.js:289 prefs.js:529
msgid "Add"
msgstr ""

#: prefs.js:461
msgid "Edit Pinned App"
msgstr ""

#: prefs.js:461
msgid "Add a Custom Shortcut"
msgstr ""

#: prefs.js:469
msgid "Shortcut Name:"
msgstr ""

#: prefs.js:484
msgid "Icon:"
msgstr ""

#: prefs.js:497 prefs.js:1151
msgid "Please select an image icon"
msgstr ""

#: prefs.js:515
msgid "Terminal Command:"
msgstr ""

#: prefs.js:568
msgid "General"
msgstr ""

#: prefs.js:575
msgid "Arc Menu Position in Panel"
msgstr ""

#: prefs.js:582
msgid "Left"
msgstr ""

#: prefs.js:585
msgid "Center"
msgstr ""

#: prefs.js:589
msgid "Right"
msgstr ""

#: prefs.js:632
msgid "Menu Alignment to Button"
msgstr ""

#: prefs.js:658
msgid "Display on all monitors when using Dash to Panel"
msgstr ""

#: prefs.js:678
msgid "Disable Tooltips"
msgstr ""

#: prefs.js:698
msgid "Disable activities hot corner"
msgstr ""

#: prefs.js:742
msgid "Hotkey activation"
msgstr ""

#: prefs.js:748
msgid "Key Release"
msgstr ""

#: prefs.js:749
msgid "Key Press"
msgstr ""

#: prefs.js:768
msgid "Arc Menu Hotkey"
msgstr ""

#: prefs.js:777
msgid "Left Super Key"
msgstr ""

#: prefs.js:783
msgid "Right Super Key"
msgstr ""

#: prefs.js:790
msgid "Custom Hotkey"
msgstr ""

#: prefs.js:797
msgid "None"
msgstr ""

#: prefs.js:855
msgid "Current Hotkey"
msgstr ""

#: prefs.js:868
msgid "Modify Hotkey"
msgstr ""

#: prefs.js:916
msgid "Set Custom Hotkey"
msgstr ""

#: prefs.js:925
msgid "Press a key"
msgstr ""

#: prefs.js:950
msgid "Modifiers"
msgstr ""

#: prefs.js:955
msgid "Ctrl"
msgstr ""

#: prefs.js:960
msgid "Super"
msgstr ""

#: prefs.js:964
msgid "Shift"
msgstr ""

#: prefs.js:968
msgid "Alt"
msgstr ""

#: prefs.js:1033 prefs.js:1722 prefs.js:1967 prefs.js:2183 prefs.js:2766
msgid "Apply"
msgstr ""

#: prefs.js:1070 prefs.js:1078 prefs.js:1380
msgid "Menu Button Appearance"
msgstr ""

#: prefs.js:1084
msgid "Icon"
msgstr ""

#: prefs.js:1085
msgid "Text"
msgstr ""

#: prefs.js:1086
msgid "Icon and Text"
msgstr ""

#: prefs.js:1087
msgid "Text and Icon"
msgstr ""

#: prefs.js:1101
msgid "Menu Button Text"
msgstr ""

#: prefs.js:1122
msgid "Arrow beside Menu Button"
msgstr ""

#: prefs.js:1141
msgid "Menu Button Icon"
msgstr ""

#: prefs.js:1156
msgid "Arc Menu Icon"
msgstr ""

#: prefs.js:1157
msgid "Arc Menu Alt Icon"
msgstr ""

#: prefs.js:1158
msgid "System Icon"
msgstr ""

#: prefs.js:1159
msgid "Custom Icon"
msgstr ""

#: prefs.js:1197
msgid "Icon Size"
msgstr ""

#: prefs.js:1229
msgid "Icon Padding"
msgstr ""

#: prefs.js:1260
msgid "Icon Color"
msgstr ""

#: prefs.js:1281
msgid "Active Icon Color"
msgstr ""

#: prefs.js:1301
msgid ""
"Icon color options will only work with files ending with \"-symbolic.svg\""
msgstr ""

#: prefs.js:1314 prefs.js:1945 prefs.js:2721
msgid "Reset"
msgstr ""

#: prefs.js:1360
msgid "Appearance"
msgstr ""

#: prefs.js:1407 prefs.js:1782
msgid "Customize Arc Menu Appearance"
msgstr ""

#: prefs.js:1455 prefs.js:2328
msgid "Override Arc Menu Theme"
msgstr ""

#: prefs.js:1514
msgid "Current Color Theme"
msgstr ""

#: prefs.js:1539
msgid "Menu Layout"
msgstr ""

#: prefs.js:1597
msgid "Current Layout"
msgstr ""

#: prefs.js:1689
msgid "Menu style chooser"
msgstr ""

#: prefs.js:1698
msgid "Arc Menu Layout"
msgstr ""

#: prefs.js:1795
msgid "Menu Height"
msgstr ""

#: prefs.js:1827
msgid "Left-Panel Width"
msgstr ""

#: prefs.js:1852
msgid "Large Application Icons"
msgstr ""

#: prefs.js:1871 prefs.js:2677
msgid "Enable Vertical Separator"
msgstr ""

#: prefs.js:1890 prefs.js:2698
msgid "Separator Color"
msgstr ""

#: prefs.js:1912
msgid "Fine Tune"
msgstr ""

#: prefs.js:1920
msgid "Gap Adjustment"
msgstr ""

#: prefs.js:2005
msgid "Color Theme Name"
msgstr ""

#: prefs.js:2012
msgid "Name:"
msgstr ""

#: prefs.js:2037
msgid "Save Theme"
msgstr ""

#: prefs.js:2062
msgid "Select Themes to Import"
msgstr ""

#: prefs.js:2062
msgid "Select Themes to Export"
msgstr ""

#: prefs.js:2080
msgid "Import"
msgstr ""

#: prefs.js:2080
msgid "Export"
msgstr ""

#: prefs.js:2111
msgid "Select All"
msgstr ""

#: prefs.js:2168
msgid "Manage Themes"
msgstr ""

#: prefs.js:2341
msgid "Color Theme Presets"
msgstr ""

#: prefs.js:2351
msgid "Save as Preset"
msgstr ""

#: prefs.js:2435
msgid "Manage Presets"
msgstr ""

#: prefs.js:2464
msgid "Menu Background Color"
msgstr ""

#: prefs.js:2486
msgid "Menu Foreground Color"
msgstr ""

#: prefs.js:2506
msgid "Font Size"
msgstr ""

#: prefs.js:2532
msgid "Border Color"
msgstr ""

#: prefs.js:2553
msgid "Border Size"
msgstr ""

#: prefs.js:2579
msgid "Highlighted Item Color"
msgstr ""

#: prefs.js:2600
msgid "Corner Radius"
msgstr ""

#: prefs.js:2626
msgid "Menu Arrow Size"
msgstr ""

#: prefs.js:2652
msgid "Menu Displacement"
msgstr ""

#: prefs.js:2831
msgid "Shortcuts"
msgstr ""

#: prefs.js:2880
msgid "External Devices"
msgstr ""

#: prefs.js:2902
msgid "Bookmarks"
msgstr ""

#: prefs.js:3042
msgid "Misc"
msgstr ""

#: prefs.js:3048
msgid "Export and Import Arc Menu Settings"
msgstr ""

#: prefs.js:3055
msgid "Importing settings from file may replace ALL saved settings."
msgstr ""

#: prefs.js:3056
msgid "This includes all saved pinned apps."
msgstr ""

#: prefs.js:3065
msgid "Import from File"
msgstr ""

#: prefs.js:3071
msgid "Import settings"
msgstr ""

#: prefs.js:3100
msgid "Export to File"
msgstr ""

#: prefs.js:3107
msgid "Export settings"
msgstr ""

#: prefs.js:3138
msgid "Color Theme Presets - Export/Import"
msgstr ""

#: prefs.js:3157
msgid "Imported theme presets are located on the Appearance Tab"
msgstr ""

#: prefs.js:3158
msgid "in Override Arc Menu Theme"
msgstr ""

#: prefs.js:3169 prefs.js:3175
msgid "Import Theme Preset"
msgstr ""

#: prefs.js:3212 prefs.js:3223
msgid "Export Theme Preset"
msgstr ""

#: prefs.js:3288
msgid "About"
msgstr ""

#: prefs.js:3315
msgid "Arc-Menu"
msgstr ""

#: prefs.js:3320
msgid "version: "
msgstr ""

#: prefs.js:3324
msgid "A traditional application menu for GNOME"
msgstr ""

#: prefs.js:3328
msgid "GitLab Page"
msgstr ""

#: menu.js:568
msgid "Arc Menu on GitLab"
msgstr ""

#: menu.js:573
msgid "About Arc Menu"
msgstr ""

#: menu.js:581
msgid "Dash to Panel Settings"
msgstr ""
